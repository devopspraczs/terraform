terraform {
  backend "s3" {
    bucket = "my-tf-tt-bucket"
    key    = "s3/terraform.tfstate"
    region = "us-east-1"
  }
}
provider "aws" {
    profile = "default"
    region = "us-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "my-tf-tt-bucket"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}